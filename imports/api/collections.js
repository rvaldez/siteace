// we need to use the Mongo compoent
// so we need to import it
import { Mongo } from 'meteor/mongo';

Websites = new Mongo.Collection("websites");
Comments = new Mongo.Collection("comments");
Meteor.methods({  
  commentInsert: function(commentAttributes) {
      var user = Meteor.user();
      var website = Websites.findOne(commentAttributes.websiteId);
      if (!website)
        throw new Meteor.Error('invalid-comment', 'You must comment on a website');
        comment = _.extend(commentAttributes, { 
            userId: user._id,
            author: user.username, 
            submitted: new Date()
        });
        return Comments.insert(comment);
      }});
