import { Meteor } from 'meteor/meteor';
import {http} from 'meteor/http';
import '/imports/api/collections.js';
// start up function that creates entries in the Websites databases.
Meteor.startup(function () {
  
    // code to run on server at startup
    if (!Websites.findOne()) {
        console.log("No websites yet. Creating starter data.");
        var now = new Date().getTime();
        // create two users
        var test0Id = Meteor.users.insert({
           profile: { username: 'test0' }
        });
        var test0 = Meteor.users.findOne(test0Id);
        var test1Id = Meteor.users.insert({
            profile: { username: 'test1' }
        });
        var test1 = Meteor.users.findOne(test1Id);

        var websiteId = Websites.insert({
            title: "Meteor social website aggregator",
            url: "https://gitlab.com/rvaldez/siteace",
            description: "Peer Assessment SiteAce",
            createdOn: new Date(now - 6 * 3600 * 1000),
            createdBy: test0._id,
            upvotes: 0,
            downvotes: 0
        });
        Comments.insert({
            websiteId: websiteId,
            userId: test0._id,
            author: test0.profile.username,
            submitted: new Date(now - 5 * 3600 * 1000),
            body: 'Interesting project Test1, can I get involved?'
        });

        Comments.insert({
            websiteId: websiteId,
            userId: test1._id,
            author: test1.profile.username,
            submitted: new Date(now - 3 * 3600 * 1000),
            body: 'You sure can Test0!'
        });

        Websites.insert({
            title: "Goldsmiths Computing Department",
            url: "http://www.gold.ac.uk/computing/",
            description: "This is where this course was developed.",
            createdOn: new Date(),
            createdBy: test0.profile.username,
            upvotes : 0,
            downvotes : 0
        });
        Websites.insert({
            title: "University of London",
            url: "http://www.londoninternational.ac.uk/courses/undergraduate/goldsmiths/bsc-creative-computing-bsc-diploma-work-entry-route",
            description: "University of London International Programme.",
            createdOn: new Date(),
            createdBy: test0.profile.username,
            upvotes : 0,
            downvotes : 0
        });
        Websites.insert({
            title: "Coursera",
            url: "http://www.coursera.org",
            description: "Universal access to the world’s best education.",
            createdOn: new Date(),
            createdBy: test0.profile.username,
            upvotes : 0,
            downvotes : 0
        });
        Websites.insert({
            title: "Google",
            url: "http://www.google.com",
            description: "Popular search engine.",
            createdOn: new Date(),
            createdBy: test0.profile.username,
            upvotes : 0,
            downvotes : 0
        });
    }

Meteor.methods({
    get_webpage: function (url) {
        this.unblock();
        return Meteor.http.call("GET", url, {"npmRequestOptions" : {"gzip" : true}});
    }
  });

});