import { Accounts } from "meteor/accounts-base";
import { Router } from 'meteor/iron:router';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';


/// routing
Router.configure({
    layoutTemplate: 'SiteaceLayout'
});

Router.route('/', function () {
    this.render('navbar', {
        to: "navbar"
    });
    this.render('website_list', {
        to: "main"
    });
});
Router.route('/:_id', function () {
    this.render('navbar', {
        to: "navbar"
    });
    this.render('website_detail_page', {
        to: "main",
        waitOn: function() {
            return Meteor.subscribe('comments', this.params._id);
        },
        data: function () {
            return Websites.findOne({ _id: this.params._id });
        }
    });
});



/////
// template helpers 
/////

// helper function that returns all available websites
Template.website_list.helpers({
    websites: function () {
        return Websites.find({}, { sort: { upvotes: -1 }, limit: Session.get("websitesLimit") });
    }
});


// helper function that formats Date
Template.website_item.helpers({
    createdonText: function () {
        return this.createdOn.toISOString().substring(0, 10);
    },
    commentsCount: function () {
        return Comments.find({ websiteId: this._id }).count();
    }
});

// helper function that shows comments
Template.website_detail_page.helpers({
    comments: function () {
        return Comments.find({ websiteId: this._id });
    }
});

// helper function that formats comments
Template.commentItem.helpers({
    submittedText: function () {
        return this.submitted.toISOString().substring(0, 10) + ' at ' + this.submitted.toISOString().substring(12, 16) ;
    }
});

// helper function for comments
Template.commentSubmit.onCreated(function () {
    Session.set('commentSubmitErrors', {});
});

Template.commentSubmit.helpers({
    errorMessage: function (field) {
        return Session.get('commentSubmitErrors')[field];
    },
    errorClass: function (field) {
        return !!Session.get('commentSubmitErrors')[field] ? 'has-error' : '';
    }
});

Template.commentSubmit.events({
    'submit form': function (e, template) {
        e.preventDefault();

        var $body = $(e.target).find('[name=body]');
        var comment = {
            body: $body.val(),
            websiteId: template.data._id
        };

        var errors = {};
        if (!comment.body) {
            errors.body = "Please write some content";
            return Session.set('commentSubmitErrors', errors);
        }

        Meteor.call('commentInsert', comment, function (error, commentId) {
            if (error) {
                throwError(error.reason);
            } else {
                $body.val('');
            }
        });
    }
});


/////
// template events 
/////

Template.website_item.events({
    "click .js-upvote": function (event) {
        // example of how you can access the id for the website in the database
        // (this is the data context for the template)
        var website_id = this._id;
        //console.log("Up voting website with id " + website_id);
        // put the code in here to add a vote to a website!
        Websites.update(website_id, {
            $inc: { upvotes: 1 }
        });

        return false;// prevent the button from reloading the page
    },
    "click .js-downvote": function (event) {

        // example of how you can access the id for the website in the database
        // (this is the data context for the template)
        var website_id = this._id;
        //console.log("Down voting website with id " + website_id);

        // put the code in here to remove a vote from a website!
        Websites.update(website_id, {
            $inc: { downvotes: 1 }
        });


        return false;// prevent the button from reloading the page
    }
});

// validate url
Template.website_form.onRendered(function () {
    $(".js-save-website-form").validate({
        rules: {
            url: {
                required: true,
                url: true
            }
        }
    });
});

Template.website_form.events({
    "click .js-toggle-website-form": function (event) {
        $("#website_form").toggle('slow');
    },
    
      "submit .js-save-website-form": function(event){
        var url = event.target.url.value;

        Meteor.call("get_webpage", url, function(error, response) {
            if (error) {
                console.log(error);
            } else {
                page = "<div>" + response.content + "</div>";
                var title = $("title", page).text();
                var description = $("meta[name='description']", page).attr("content");
                console.log(title);
                console.log(description);

                if (Meteor.user()) {
                    Websites.insert({
                        url: url,
                        title: title,
                        description: description,
                        createdOn: new Date(),
                        upvotes: 0,
                        downvotes: 0,
                        comments: []
                    });
                    console.log("Added site with url " + url);
                } else {
                    alert("You must login to add a site!");
                }
}  });
    // hide and reset form after submit
            $("#website_form").toggle('slow');
            $(".js-save-website-form").trigger('reset');
            return false;// stop the form submit from reloading the page

       
}

});

/// accounts config

Accounts.ui.config({
    passwordSignupFields: "USERNAME_AND_EMAIL"
});

/// infiniscroll

Session.set("websitesLimit", 10);
lastScrollTop = 0;
$(window).scroll(function (event) {
    // test if we are near the bottom of the window
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
        // where are we in the page?
        var scrollTop = $(this).scrollTop();
        // test if we are going down
        if (scrollTop > lastScrollTop) {
            // yes we are heading down...
            Session.set("websitesLimit", Session.get("websitesLimit") + 4);
        }

        lastScrollTop = scrollTop;
    }

});
